// Componentを代表してReduxと接続するComponent

import { connect } from 'react-redux';
import * as actions from '../actions/ActionTodo';
import Todo from '../components/Todo';

// stateをpropsにマッピングする
// コンポーネントで使用するstateを切り出して、propsで参照できるようにする
const mapStateToProps = state => {
  return {
    todo: state.todo,
  }
}

// Dispatcher
// ReducerにActionを渡す
const mapDispathToProps = dispatch => {
  return {
    addTodo: (todo) => dispatch(actions.addTodo(todo)),
  }
}

// コンポーネントに接続できるようにする
export default connect(mapStateToProps, mapDispathToProps)(Todo)