const initialState = {
  todoList: []
};

export const todoReducer = (state = initialState, action) => {
  switch(action.type){
    case 'ADD_TODO':
       // 新しく追加するtodo
       const todo = action.payload.todo;

       // stateを複製する
       const newState = Object.assign({}, state);

       // 複製したstateに新しく追加するtodoを追加する
       newState.todoList.push(todo);

       // 新しいstateを返す
       return newState;
    default:
       return state
  }
};