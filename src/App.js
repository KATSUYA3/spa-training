import React, { Component } from 'react';
import './App.css';

//import Todo from './components/Todo';
import Todo from './containers/Container';

class App extends Component {
  render() {
    return (
      <div className="App">
         <Todo />
      </div>
    );
  }
}

export default App;
